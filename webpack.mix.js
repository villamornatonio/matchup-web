let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */
//
mix.js('resources/js/app.js', 'public/js')
    .copy('resources/theme/css/quirk.css', 'public/css/theme.css')
    .copy('resources/theme/lib', 'public/vendor')
    .copy('resources/theme/fonts', 'public/fonts')
    .copy('resources/theme/images', 'public/images')
    .copy('resources/theme/js', 'public/js')
    .extract(['vue', 'vue-router', 'vuex', 'axios']);
mix.sourceMaps();