import VueRouter from 'vue-router';

const routes = [
    {
        path: '/',
        component: require('./vue/layouts/pages'),
        meta: {requiresAuth: false},
        children: [
            {
                path: '',
                component: require('./vue/pages/home'),
                meta: {requiresAuth: false}
            }
        ]
    },
     {
        path: '/about',
        component: require('./vue/layouts/pages'),
        meta: {requiresAuth: false},
        children: [
            {
                path: '',
                component: require('./vue/pages/about'),
                meta: {requiresAuth: false}
            }
        ]
    },
     {
        path: '/contact',
        component: require('./vue/layouts/pages'),
        meta: {requiresAuth: false},
        children: [
            {
                path: '',
                component: require('./vue/pages/contact'),
                meta: {requiresAuth: false}
            }
        ]
    },
    {
        path: '/login',
        component: require('./vue/layouts/portal'),
        meta: {requiresAuth: false},
        children: [
            {
                path: '',
                component: require('./vue/auth/login'),
                meta: {requiresAuth: false}
            }
        ]
    },
    {
        path: '/profile',
        component: require('./vue/layouts/admin'),
        meta: {requiresAuth: true},
        children: [
            {
                path: '',
                component: require('./vue/auth/profile'),
                meta: {requiresAuth: true}
            }
        ]
    },
    {
        path: '/account-setting',
        component: require('./vue/layouts/admin'),
        meta: {requiresAuth: true},
        children: [
            {
                path: '',
                component: require('./vue/auth/account-settings'),
                meta: {requiresAuth: true}
            }
        ]
    },
     {
        path: '/dashboard',
        component: require('./vue/layouts/admin'),
        meta: {requiresAuth: true},
        children: [
            {
                path: '',
                component: require('./vue/dashboard/index'),
                meta: {requiresAuth: true}
            }
        ]
    },
    {
        path: '/users',
        component: require('./vue/layouts/admin'),
        meta: {requiresAuth: true},
        children: [
            {
                path: '',
                component: require('./vue/users/index'),
                meta: {requiresAuth: true}
            },
            {
                path: 'create',
                component: require('./vue/users/create'),
                meta: {requiresAuth: true}
            }
        ]
    },
    {
        path: '/announcements',
        component: require('./vue/layouts/admin'),
        meta: {requiresAuth: true},
        children: [
            {
                path: '',
                component: require('./vue/announcements/index'),
                meta: {requiresAuth: true}
            },
            {
                path: 'create',
                component: require('./vue/announcements/create'),
                meta: {requiresAuth: true}
            }
            ,
            {
                path: ':id/edit',
                component: require('./vue/announcements/edit'),
                meta: {requiresAuth: true}
            }
        ]
    },
    {
        path: '/teachers',
        component: require('./vue/layouts/admin'),
        meta: {requiresAuth: true},
        children: [
            {
                path: '',
                component: require('./vue/teachers/index'),
                meta: {requiresAuth: true}
            },
            {
                path: 'create',
                component: require('./vue/teachers/create'),
                meta: {requiresAuth: true}
            }
            ,
            {
                path: ':id/edit',
                component: require('./vue/teachers/edit'),
                meta: {requiresAuth: true}
            }
        ]
    },
    {
        path: '/guardians',
        component: require('./vue/layouts/admin'),
        meta: {requiresAuth: true},
        children: [
            {
                path: '',
                component: require('./vue/guardians/index'),
                meta: {requiresAuth: true}
            },
            {
                path: 'create',
                component: require('./vue/guardians/create'),
                meta: {requiresAuth: true}
            }
            ,
            {
                path: ':id/edit',
                component: require('./vue/guardians/edit'),
                meta: {requiresAuth: true}
            }
        ]
    },
    {
        path: '/students',
        component: require('./vue/layouts/admin'),
        meta: {requiresAuth: true},
        children: [
            {
                path: '',
                component: require('./vue/students/index'),
                meta: {requiresAuth: true}
            },
            {
                path: 'create',
                component: require('./vue/students/create'),
                meta: {requiresAuth: true}
            }
            ,
            {
                path: ':id/edit',
                component: require('./vue/students/edit'),
                meta: {requiresAuth: true}
            }
        ]
    },
    {
        path: '/subjects',
        component: require('./vue/layouts/admin'),
        meta: {requiresAuth: true},
        children: [
            {
                path: '',
                component: require('./vue/subjects/index'),
                meta: {requiresAuth: true}
            },
            {
                path: 'create',
                component: require('./vue/subjects/create'),
                meta: {requiresAuth: true}
            }
            ,
            {
                path: ':id/edit',
                component: require('./vue/subjects/edit'),
                meta: {requiresAuth: true}
            }
        ]
    },
    {
        path: '/classes',
        component: require('./vue/layouts/admin'),
        meta: {requiresAuth: true},
        children: [
            {
                path: '',
                component: require('./vue/classes/index'),
                meta: {requiresAuth: true}
            },
            {
                path: 'create',
                component: require('./vue/classes/create'),
                meta: {requiresAuth: true}
            }
            ,
            {
                path: ':id/edit',
                component: require('./vue/classes/edit'),
                meta: {requiresAuth: true}
            }
        ]
    },
    {
        path: '/settings',
        component: require('./vue/layouts/admin'),
        meta: {requiresAuth: true},
        children: [
            {
                path: '',
                component: require('./vue/settings/index'),
                meta: {requiresAuth: true}
            }
        ]
    },
    {
        path: '/notifications',
        component: require('./vue/layouts/admin'),
        meta: {requiresAuth: true},
        children: [
            {
                path: '',
                component: require('./vue/notifications/index'),
                meta: {requiresAuth: true}
            },
            {
                path: ':id/show',
                component: require('./vue/notifications/show'),
                meta: {requiresAuth: true}
            }
        ]
    },
    {
        path: '*',
        component: require('./vue/layouts/errors'),
        meta: {requiresAuth: false},
        children: [
            {
                path: '',
                component: require('./vue/errors/404'),
                meta: {requiresAuth: false}
            }
        ]
    }

];

const router = new VueRouter({
    linkActiveClass: "active",
    linkExactActiveClass: "active",
    mode: 'history',
    routes
});

router.beforeEach((to, from, next) => {
    let accessToken = localStorage.getItem('access_token');
    if (to.meta.requiresAuth) {
        if (!accessToken || accessToken === null) {
            next({
                path: '/login',
                query: {redirect: to.fullPath}
            });
        }
    }
    next();


});

export default router;