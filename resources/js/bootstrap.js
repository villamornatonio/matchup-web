import Vue from 'vue';
import VueRouter from 'vue-router';
import axios from 'axios';
import Form from './core/Form';
import VueSwal from 'vue-swal';

window.Vue = Vue;
window.Form = Form;
Vue.use(VueSwal);
Vue.use(VueRouter);
Vue.use(require('vuejs-datatable'));


window.axios = axios;

window.axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest'
}
// window.axios.defaults.baseURL = 'https://api-matchup.herokuapp.com';
window.axios.defaults.baseURL = 'http://api.matchup.com';

axios.interceptors.request.use((config) => {

    let accessToken;
    accessToken = localStorage.getItem('access_token');


    if (accessToken !== null && accessToken !== 'undefined') {
        config.headers.common['Authorization'] = 'Bearer ' + accessToken;
    }
    return config
}, function (error) {
    // Do something with request error
    return Promise.reject(error)
});

axios.interceptors.response.use(function (response) {
    return response;
}, function (error) {

    const originalRequest = error.config;

    if (error.response.status === 401 && !originalRequest._retry) {

        originalRequest._retry = true;

        const refreshToken = window.localStorage.getItem('refresh_token');
        return axios.post('/api/auth/refresh', { refreshToken })
            .then(({data}) => {
                window.localStorage.setItem('access_token', data.access_token);
                window.localStorage.setItem('refresh_token', data.refresh_token);
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + data.access_token;
                originalRequest.headers['Authorization'] = 'Bearer ' + data.access_token;
                return axios(originalRequest);
            });
    }

    return Promise.reject(error);
});

// Declare Components Here

Vue.component('top-navigation-component',require('./vue/components/partials/top-navigation-component.vue'));
Vue.component('sidebar-component',require('./vue/components/partials/sidebar-component.vue'));






