import './bootstrap.js';

import router from './routes.js';
import store from './store.js';

new Vue({
    el: '#root',
    router : router,
    store:store,
    data: {
        shared: store
    },
    mounted(){
        let that = this;
        that.$on('notifySuccess', function (message) {
            $.gritter.add({
                title: 'Success',
                text: message,
                class_name: 'with-icon check-circle success'
            });
        });
        that.$on('notifyFailed', function (message) {
            $.gritter.add({
                title: 'Something went wrong!',
                text: message,
                class_name: 'with-icon times-circle danger'
            });
        });
        that.$on('notifyInformation', function (message) {
            $.gritter.add({
                title: 'Information',
                text: message,
                class_name: 'with-icon question-circle primary'
            });
        });
    },
    created(){
        var accessToken = localStorage.getItem('access_token');
        if (accessToken !== null && accessToken !== 'undefined') {
            var that = this;
            axios.get('/api/user')
                .then(function(response){
                    that.setLogin(response.data.data);
                })
                .catch(function(error){
                    that.destroyLogin();
                });
        }

    },
    methods:{
        setLogin(user) {
            let that = this;
            // Save login info in our data and set header in case it's not set already
            this.$store.state.user = user;
            this.$store.state.isAuthenticated = true;
            this.$store.state.access_token = localStorage.getItem('access_token');
        },

        destroyLogin() {
            let that = this;
            // Cleanup when token was invalid our user has logged out
            this.$store.state.user = {};
            this.$store.state.isAuthenticated = false;
            this.$store.state.access_token = null;
            localStorage.removeItem('access_token');
            localStorage.removeItem('refresh_token');
            that.$router.push('/dashboard');

        },
    }
});
